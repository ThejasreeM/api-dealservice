/**
 * User Repo is an interface to manage JPA methods like save(),delete(), deleteAll(), findAll(), ..
 */
package com.wavelabs.in.repo;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.in.model.User;

/**
 * @author thejasree
 *
 */
public interface UserRepository extends CrudRepository<User, String> {

	void deleteById(long id);

	User findById(long id);

	User findUserById(long userId);

	User findUserByMobile(long mobileNumber);

	User findUserByEmailAndMobile(String email, long mobile);

}
