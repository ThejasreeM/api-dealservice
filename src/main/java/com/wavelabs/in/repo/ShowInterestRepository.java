/**
 * 
 */
package com.wavelabs.in.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.in.model.ShowInterest;

/**
 * @author thejasree
 *
 */
public interface ShowInterestRepository extends CrudRepository<ShowInterest, String> {

	ShowInterest findByuserIdAndDealId(long userId, long dealId);

	List<ShowInterest> findAllUsersByDealId(long dealId);

	List<ShowInterest> findAllDealsByUserId(long userId);

	ShowInterest findById(long id);

	ShowInterest findByDealIdAndUserId(long dealId, long userId);

}
