/**
 * Deal Repo is an interface to manage JPA methods like save(),delete(), deleteAll(), findAll(), ..
 */
package com.wavelabs.in.repo;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.in.model.Deal;

/**
 * @author thejasree
 *
 */
public interface DealRepository extends CrudRepository<Deal, String> {

	Deal findById(long id);

	int deleteById(long id);

	Deal findDealById(long dealId);

}
