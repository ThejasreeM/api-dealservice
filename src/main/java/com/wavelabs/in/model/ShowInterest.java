/**
 * 
 */
package com.wavelabs.in.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author thejasree
 *
 */
@Entity(name = "show_interest")
public class ShowInterest {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	public long userId;
	public long dealId;
	public boolean isShownInterest;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getDealId() {
		return dealId;
	}

	public void setDealId(long dealId) {
		this.dealId = dealId;
	}

	public boolean isShownInterest() {
		return isShownInterest;
	}

	public void setisShownInterest(boolean isShownInterest) {
		this.isShownInterest = isShownInterest;
	}

}
