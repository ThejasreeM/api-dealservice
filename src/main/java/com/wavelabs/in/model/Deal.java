/**
 * Deal POJO class to define properties of a deal.
 */
package com.wavelabs.in.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author thejasree
 *
 */
@Entity
public class Deal {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String company;
	private String website;
	private String location;
	private String image;
	private String title;
	@Column(columnDefinition = "TEXT")
	private String description;
	private String industry;
	private Double min_investment;
	private Integer founded;
	private Integer employees;
	private Double raising;
	private Double per;
	private Double annual_revenue;
	private Double raised_amount;
	private Double previous_capital;
	private String type;
	private Date start_date;
	private Date end_date;
	private String status;

	public Deal() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public Double getMin_investment() {
		return min_investment;
	}

	public void setMin_investment(Double min_investment) {
		this.min_investment = min_investment;
	}

	public Integer getFounded() {
		return founded;
	}

	public void setFounded(Integer founded) {
		this.founded = founded;
	}

	public Integer getEmployees() {
		return employees;
	}

	public void setEmployees(Integer employees) {
		this.employees = employees;
	}

	public Double getRaising() {
		return raising;
	}

	public void setRaising(Double raising) {
		this.raising = raising;
	}

	public Double getPer() {
		return per;
	}

	public void setPer(Double per) {
		this.per = per;
	}

	public Double getAnnual_revenue() {
		return annual_revenue;
	}

	public void setAnnual_revenue(Double annual_revenue) {
		this.annual_revenue = annual_revenue;
	}

	public Double getPrevious_capital() {
		return previous_capital;
	}

	public void setPrevious_capital(Double previous_capital) {
		this.previous_capital = previous_capital;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getRaised_amount() {
		return raised_amount;
	}

	public void setRaised_amount(Double raised_amount) {
		this.raised_amount = raised_amount;
	}

}
