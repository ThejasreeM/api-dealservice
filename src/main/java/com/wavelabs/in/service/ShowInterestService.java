/**
 * 
 */
package com.wavelabs.in.service;

import javax.mail.MessagingException;

import com.wavelabs.in.model.ShowInterest;

/**
 * @author thejasree
 *
 */
public interface ShowInterestService {

	boolean showInterestOnDeal(long dealId, long userId) throws MessagingException;

	ShowInterest getShowInterestByUserAndDeal(long userId, long dealId);

}
