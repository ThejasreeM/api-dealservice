package com.wavelabs.in.service;

import java.util.List;

import com.wavelabs.in.model.User;

public interface UserService {
	
	public boolean addUser();
	public User editUser(long id, User user);
	public boolean removeUser();
	public User getUser(long id);
	public List<User> getAllUsers();

}
