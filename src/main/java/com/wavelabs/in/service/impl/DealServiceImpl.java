package com.wavelabs.in.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.wavelabs.in.model.Deal;
import com.wavelabs.in.repo.DealRepository;

@Component
@Transactional
public class DealServiceImpl {
	@Autowired
	DealRepository dealRepo;

	/*
	 * add user
	 */
	public boolean addDeal(Deal deal) {
		try {
			dealRepo.save(deal);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * update user
	 */
	public Deal updateDeal(long id, Deal deal) {
		try {
			deal.setId(id);
			return dealRepo.save(deal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * get user
	 */
	public Deal getDeal(long id) {
		try {
			return dealRepo.findById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * get all users
	 */
	public List<Deal> getAllDeals() {
		try {
			return (List<Deal>) dealRepo.findAll();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * remove user
	 */
	public Boolean removeDeal(long id) {
		try {
			dealRepo.deleteById(id);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
/*
 * update status for a particualr deal
 */
	public boolean addStatus(long dealId, String status) {
		try {
			Deal d = dealRepo.findById(dealId);
			System.out.println(d.toString());
			d.setStatus(status);
			dealRepo.save(d);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
