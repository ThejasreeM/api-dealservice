/**
 * 
 */
package com.wavelabs.in.service.impl;

import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.wavelabs.in.model.Deal;
import com.wavelabs.in.model.ShowInterest;
import com.wavelabs.in.model.User;
import com.wavelabs.in.repo.DealRepository;
import com.wavelabs.in.repo.ShowInterestRepository;
import com.wavelabs.in.repo.UserRepository;
import com.wavelabs.in.service.ShowInterestService;

/**
 * @author thejasree
 *
 */

@Component
@Transactional
public class ShowInterestServiceImpl implements ShowInterestService {
	@Autowired
	DealRepository dealRepo;
	@Autowired
	UserRepository userRepo;
	@Autowired
	ShowInterestRepository showInterestRepo;
	@Autowired
	JavaMailSender sender;

	Logger logger = Logger.getLogger(ShowInterestServiceImpl.class);

	@Override
	public boolean showInterestOnDeal(long dealId, long userId) throws MessagingException {
		try {
			if (getShowInterestByUserAndDeal(userId, dealId) == null) {
				ShowInterest showInterest = new ShowInterest();
				showInterest.setDealId(dealId);
				showInterest.setUserId(userId);
				showInterest.setisShownInterest(true);
				showInterestRepo.save(showInterest);
				/**
				 * preparing email content with user name and deal and sending email
				 */
				prepareEmailContentAndSendEmail(userId, dealId);
				return true;
			}
		} catch (Exception e) {
			logger.error("error while interest and sending email");
			e.printStackTrace();
		}
		return false;

	}

	/**
	 * This method prepares the email content sends email
	 * 
	 * @param userId
	 * @param dealId
	 */
	public void prepareEmailContentAndSendEmail(long userId, long dealId) {
		String uname = null;
		String dname = null;
		Deal deal = null;
		User user = null;
		try {
			deal = dealRepo.findById(dealId);
			user = userRepo.findById(userId);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error while fetching deal and user");
		}
		dname = (deal != null) ? deal.getTitle() : null;
		logger.info("DealNAme - " + dname);
		uname = (user != null) ? user.getName() : null;
		logger.info("Name-" + uname);
		if (uname != null && dname != null) {
			try {
				logger.info("success sent a mail");
				sendEmail(uname, dname);
			} catch (MessagingException e) {
				e.printStackTrace();
				logger.error("someting went wrong while sending eamil");
			}
		}
	}

	/**
	 * sendEmail() method sends an email to Admin about the people who are shown
	 * interest on a deal.
	 * 
	 * @param username
	 * @param dealname
	 * @throws MessagingException
	 */
	private void sendEmail(String username, String dealName) throws MessagingException {
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		helper.setTo("info@50kventures.com");
		helper.setText(username + " shown interest on a deal: " + dealName);
		helper.setSubject("Shows Interest on Deal");
		sender.send(message);
	}

	/**
	 * getShowInterestByUserAndDeal
	 */
	@Override
	public ShowInterest getShowInterestByUserAndDeal(long userId, long dealId) {
		try {
			return showInterestRepo.findByuserIdAndDealId(userId, dealId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * get userids of interested people on deal
	 */
	public List<ShowInterest> getInterestedPeopleOnDeal(long dealId) {
		try {
			return showInterestRepo.findAllUsersByDealId(dealId);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * get list of interested people on deal.
	 */

	public User getNameAndImage(long userId) {
		try {
			return userRepo.findUserById(userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/*
	 * get list of deals interested by the user
	 */

	public List<ShowInterest> getInterestedDealsOfUser(long userId) {
		try {
			return showInterestRepo.findAllDealsByUserId(userId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	/*
	 * get list of deals interested by the user
	 */

	public Deal getTitleAndImage(long dealId) {
		try {
			return dealRepo.findDealById(dealId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * get is shown interest value
	 */
	public ShowInterest getisShownInterest(long dealId, long userId) {
		try {
			return showInterestRepo.findByDealIdAndUserId(dealId, userId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

}
