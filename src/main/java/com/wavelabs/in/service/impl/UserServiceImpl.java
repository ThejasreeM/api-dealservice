package com.wavelabs.in.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.wavelabs.in.model.User;
import com.wavelabs.in.repo.UserRepository;

@Component
@Transactional
public class UserServiceImpl {
	@Autowired
	UserRepository userRepo;

	/*
	 * add user
	 */
	public boolean addUser(User user) {
		try {
			userRepo.save(user);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * update user
	 */
	public User updateUser(User user, long id) {
		try {
			user.setId(id);
			return userRepo.save(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * get user
	 */
	public User getUser(long id) {
		try {
			return userRepo.findById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * get all users
	 */
	public List<User> getAllUsers() {
		try {
			return (List<User>) userRepo.findAll();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * remove user
	 */
	public Boolean removeUser(long id) {
		try {
			userRepo.deleteById(id);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * get user based on mobile number
	 */
//	public User getUserByMobile(long mobileNumber) {
//		try {
//			return userRepo.findUserByMobile(mobileNumber);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

	/*
	 * get user based on mobile and email
	 */
	public User getUserByMobileAndEmail(long mobile, String email) {
		try {
			return userRepo.findUserByEmailAndMobile(email, mobile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
