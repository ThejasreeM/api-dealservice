package com.wavelabs.in.service;

import java.util.List;

import com.wavelabs.in.model.Deal;

public interface DealService {
	
	public boolean addDeal();
	public Deal editDeal(Deal deal, long id);
	public Deal getDeal(long id);
	public List<Deal> getAllDeals();
	public boolean removeDeal();

}
