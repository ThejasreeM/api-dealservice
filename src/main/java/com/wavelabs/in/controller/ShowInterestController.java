/**
 * ShowInterestController can control CRUD responses.
 */
package com.wavelabs.in.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.in.model.Deal;
import com.wavelabs.in.model.ShowInterest;
import com.wavelabs.in.model.User;
import com.wavelabs.in.service.impl.ShowInterestServiceImpl;

import io.nbos.capi.api.v0.models.RestMessage;

/**
 * @author thejasree
 *
 */
@RestController
@Component
@CrossOrigin
@RequestMapping(path = "/api/deal")
public class ShowInterestController {

	@Autowired
	ShowInterestServiceImpl showInterestServiceImpl;

	RestMessage restMessage = new RestMessage();
	/*
	 * user shows interest on a deal response
	 */

	@RequestMapping(value = "/{dealId}/interest/{userId}", method = RequestMethod.POST)
	public ResponseEntity<Map<Object, Object>> showInterestOnDeal(@PathVariable long dealId, @PathVariable long userId)
			throws MessagingException {
		boolean status = showInterestServiceImpl.showInterestOnDeal(dealId, userId);
		Map<Object, Object> response = new HashMap<Object, Object>();
		if (status) {
			response.put("messageCode", 200);
			response.put("message", "User shows interest on a deal");
			response.put("is_shown_interest", status);
			return ResponseEntity.status(200).body(response);
		}
		response.put("messageCode", 200);
		response.put("message", "User shows interest on a deal is unsuccess");
		response.put("is_shown_interest", status);
		return ResponseEntity.status(200).body(response);

	}

	/*
	 * get list of interested people on a particular deal
	 */
	@RequestMapping(value = "/interested/members/{dealId}", method = RequestMethod.GET)
	public ResponseEntity<?> getInterestedPeopleOnDeal(@PathVariable long dealId) {
		List<ShowInterest> interestedUsers = showInterestServiceImpl.getInterestedPeopleOnDeal(dealId);
		List<User> users = new ArrayList<>();
		for (ShowInterest strTemp : interestedUsers) {
			User user = showInterestServiceImpl.getNameAndImage(strTemp.getUserId());
			users.add(user);
		}
		if (users.isEmpty()) {
			return ResponseEntity.status(200).body(users);
		}
		return ResponseEntity.status(200).body(users);
	}

	/*
	 * get list of deals that are interested by the user
	 */

	@RequestMapping(value = "/interested/deals/{userId}", method = RequestMethod.GET)
	public ResponseEntity<?> getInterestedDealsOfUser(@PathVariable long userId) {
		List<ShowInterest> interestedDeals = showInterestServiceImpl.getInterestedDealsOfUser(userId);
		List<Deal> deals = new ArrayList<>();
		for (ShowInterest strTemp : interestedDeals) {
			Deal deal = showInterestServiceImpl.getTitleAndImage(strTemp.getDealId());
			deals.add(deal);
		}
		if (deals.isEmpty()) {
			return ResponseEntity.status(200).body(deals);
		}
		return ResponseEntity.status(200).body(deals);
	}
	/*
	 * get shown interest value by dealId and userId
	 * 
	 */

	@RequestMapping(value = "/{dealId}/{userId}", method = RequestMethod.GET)
	public ResponseEntity<?> getShownInterestValue(@PathVariable long dealId, @PathVariable long userId) {
		ShowInterest value = showInterestServiceImpl.getisShownInterest(dealId, userId);
		System.out.println(value);
		if (value  != null) {
			return ResponseEntity.status(200).body(value);
		}
//		restMessage.messageCode = "404";
//		restMessage.message = "Unable to retrieve isShownInterest value";
		return ResponseEntity.status(200).body(value);

	}

}
