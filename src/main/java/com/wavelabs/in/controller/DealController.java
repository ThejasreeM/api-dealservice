/*
 * DealController can maintain all CRUD responses.
 */
package com.wavelabs.in.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.wavelabs.in.model.Deal;
import com.wavelabs.in.model.PushNote;
import com.wavelabs.in.service.impl.DealServiceImpl;
import com.wavelabs.in.service.impl.UserServiceImpl;

import io.nbos.capi.api.v0.models.RestMessage;

/**
 * 
 * @author thejasree
 *
 */
@RestController
@Component
@CrossOrigin
@RequestMapping(path = "/api/deal")
public class DealController {

	Logger logger = Logger.getLogger(DealController.class);
	private String url = "http://19.9.8.182:8014/send";
	private String TOPIC = "Users";
	private String TYPE = "DEALS";
	private static final String FIREBASE_SERVER_KEY = "AAAApIuus8k:APA91bGOVSZOStKK0xlq3P5dcElH88QGHjE2eDZOutNrJgKUVDBYe0o-RgUvqJPJbYMLqv-tCebc7r4qqyshbbiFq9ETjFnHCNbLvuissz2btbcTslRhX8HLk09RdBgf5TkjPQS82Qlz";
	@Autowired
	DealServiceImpl dealServiceImpl;
	@Autowired
	UserServiceImpl userServiceImpl;
	RestMessage restMessage = new RestMessage();

	/*
	 * add deal response
	 */
	@RequestMapping(path = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<RestMessage> addDeal(@RequestBody Deal deal) {
		boolean status = dealServiceImpl.addDeal(deal);
		if (status) {
			// call push note service
			//boolean value = sendNotification(deal);
			//logger.info(value);
			restMessage.messageCode = "200";
			restMessage.message = "Deal has been created successfully.";
			return ResponseEntity.status(200).body(restMessage);
		} else {
			restMessage.messageCode = "404";
			restMessage.message = "Deal creation failed.";
			return ResponseEntity.status(404).body(restMessage);
		}
	}

	private boolean sendNotification(Deal deal) {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Authorization", "key=" + FIREBASE_SERVER_KEY);
		responseHeaders.set("Content-Type", "application/json");
		RestTemplate restTemplate = new RestTemplate();
		try {

			PushNote body = null;
			// Map<String, String> notification = null;
			Map<String, String> data = null;

			body = new PushNote();
			body.setTo("/topics/" + TOPIC);
			body.setPriority("high");
			logger.info("BODY:" + body);

//			notification = new HashMap<>();
//			notification.put("title", deal.getTitle());
//			notification.put("body", "Deal has been added.");
//			logger.info("NOTIFICATION:" + notification);
			// body.setNotification(notification);

			data = new HashMap<>();
			data.put("title", deal.getTitle());
			data.put("company", deal.getCompany());
			data.put("end_date", deal.getEnd_date().toString());
			data.put("description", deal.getDescription());
			data.put("image", deal.getImage());
			data.put("industry", deal.getIndustry());
			data.put("location", deal.getLocation());
			data.put("status", deal.getStatus());
			data.put("type", deal.getType());
			data.put("website", deal.getWebsite());
			data.put("start_date", deal.getStart_date().toString());
			data.put("annual_revenue", deal.getAnnual_revenue().toString());
			data.put("employees", deal.getEmployees().toString());
			data.put("min_investment", deal.getMin_investment().toString());
			data.put("per", deal.getPer().toString());
			data.put("previous_capital", deal.getPrevious_capital().toString());
			data.put("raising", deal.getRaising().toString());
			// data.put("id", deal.getId());
			data.put("type", TYPE);
			body.setData(data);

			HttpEntity<PushNote> entity = new HttpEntity<>(body);
			logger.info(body);
			restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error while sending notification");
		}
		return false;
	}

	/*
	 * edit deal response
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<RestMessage> editDeal(@RequestBody Deal deal, @PathVariable("id") long id) {
		Deal updateDeal = dealServiceImpl.updateDeal(id, deal);
		if (updateDeal == null) {
			restMessage.messageCode = "404";
			restMessage.message = "Deal updation failed.";
			return ResponseEntity.status(404).body(restMessage);
		}
		updateDeal.setAnnual_revenue(deal.getAnnual_revenue());
		updateDeal.setCompany(deal.getCompany());
		updateDeal.setDescription(deal.getDescription());
		updateDeal.setEmployees(deal.getEmployees());
		updateDeal.setEnd_date(deal.getEnd_date());
		updateDeal.setFounded(deal.getFounded());
		updateDeal.setEmployees(deal.getEmployees());
		updateDeal.setId(deal.getId());
		updateDeal.setImage(deal.getImage());
		updateDeal.setIndustry(deal.getIndustry());
		updateDeal.setMin_investment(deal.getMin_investment());
		updateDeal.setLocation(deal.getLocation());
		updateDeal.setPer(deal.getPer());
		updateDeal.setPrevious_capital(deal.getPrevious_capital());
		updateDeal.setRaising(deal.getRaising());
		updateDeal.setStart_date(deal.getStart_date());
		updateDeal.setTitle(deal.getTitle());
		updateDeal.setType(deal.getType());
		updateDeal.setWebsite(deal.getWebsite());
		updateDeal.setStatus(deal.getStatus());
		updateDeal.setRaised_amount(deal.getRaised_amount());
		dealServiceImpl.updateDeal(id, updateDeal);
		restMessage.messageCode = "200";
		restMessage.message = "Deal has been updated successfully.";
		return ResponseEntity.status(200).body(restMessage);
	}

	/*
	 * get deal response
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Deal> getDeal(@PathVariable("id") long id) {
		Deal deal = null;
		try {
			deal = dealServiceImpl.getDeal(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (deal == null) {
			return ResponseEntity.status(200).body(deal);
		}
		return ResponseEntity.status(200).body(deal);
	}

	/*
	 * get all deals response
	 */
	@RequestMapping(path = "/all", method = RequestMethod.GET)
	public ResponseEntity<?> getAllDeals() {
		List<Deal> allDeals = null;
		try {
			allDeals = dealServiceImpl.getAllDeals();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (allDeals.isEmpty()) {
			return ResponseEntity.status(200).body(allDeals);
		}
		return ResponseEntity.status(200).body(allDeals);
	}

	/*
	 * remove deal response
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<RestMessage> removeDeal(@PathVariable("id") long id) {
		boolean status = dealServiceImpl.removeDeal(id);
		if (status) {
			restMessage.message = "Deal deletion success.";
			restMessage.messageCode = "200";
			return ResponseEntity.status(200).body(restMessage);
		}
		restMessage.messageCode = "404";
		restMessage.message = "Deal deletion failed.";
		return ResponseEntity.status(404).body(restMessage);
	}

	/*
	 * update status response
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<RestMessage> addStatus(@PathVariable("id") long dealId,
			@RequestParam(name = "status") String status) {
		Boolean flag = dealServiceImpl.addStatus(dealId, status);
		logger.info(flag);
		if (flag) {
			restMessage.messageCode = "200";
			restMessage.message = "Deal status update success.";
			return ResponseEntity.status(200).body(restMessage);
		}
		restMessage.messageCode = "404";
		restMessage.message = "Deal status update failed.";
		return ResponseEntity.status(404).body(restMessage);
	}

}
