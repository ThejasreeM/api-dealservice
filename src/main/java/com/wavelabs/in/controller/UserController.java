/**
 * UserController can control all CRUD responses.
 */
package com.wavelabs.in.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.in.model.User;
import com.wavelabs.in.service.impl.UserServiceImpl;

import io.nbos.capi.api.v0.models.RestMessage;

/**
 * @author thejasree
 *
 */
@RestController
@Component
@CrossOrigin
@RequestMapping(path = "/api/user")
public class UserController {

	@Autowired
	UserServiceImpl userServiceImpl;
	RestMessage restMessage = new RestMessage();

	/*
	 * add user response
	 */
	@RequestMapping(path = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<RestMessage> addUser(@RequestBody User user) {
		boolean status = userServiceImpl.addUser(user);
		if (status) {
			restMessage.messageCode = "200";
			restMessage.message = "User has been created successfully.";
			return ResponseEntity.status(200).body(restMessage);
		} else {
			restMessage.messageCode = "404";
			restMessage.message = "User creation failed.";
			return ResponseEntity.status(404).body(restMessage);
		}
	}

	/*
	 * edit user response
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> editUser(@RequestBody User user, @PathVariable("id") long id) {
		User updateUser = userServiceImpl.updateUser(user, id);
		if (updateUser == null) {
			restMessage.messageCode = "404";
			restMessage.message = "User updation failed.";
			return ResponseEntity.status(404).body(restMessage);
		}
		//updateUser.setAge(user.getAge());
		updateUser.setEmail(user.getEmail());
		//updateUser.setGender(user.getGender());
		updateUser.setId(user.getId());
		updateUser.setImage(user.getImage());
		updateUser.setLinkedinId(user.getLinkedinId());
		updateUser.setMobile(user.getMobile());
		updateUser.setName(user.getName());
		updateUser.setTwitterId(user.getTwitterId());
		updateUser.setProfileSummary(user.getProfileSummary());
		userServiceImpl.updateUser(updateUser, id);
		restMessage.messageCode = "200";
		restMessage.message = "User has been updated successfully.";
		return ResponseEntity.status(200).body(restMessage);
	}

	/*
	 * get user response
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<User> getUser(@PathVariable("id") long id) {
		User user = userServiceImpl.getUser(id);
		if (user == null) {
			return ResponseEntity.status(200).body(user);
		}
		return ResponseEntity.status(200).body(user);
	}

	/*
	 * get all users response
	 */
	@RequestMapping(path = "/all", method = RequestMethod.GET)
	public ResponseEntity<?> getAllDeals() {
		List<User> allUsers = userServiceImpl.getAllUsers();
		if (allUsers.isEmpty()) {
			return ResponseEntity.status(200).body(allUsers);
		}
		return ResponseEntity.status(200).body(allUsers);
	}

	/*
	 * remove user response
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<RestMessage> removeUser(@PathVariable("id") long id) {
		boolean status = userServiceImpl.removeUser(id);
		if (status) {
			restMessage.message = "User deletion success.";
			restMessage.messageCode = "200";
			return ResponseEntity.status(200).body(restMessage);
		}
		restMessage.messageCode = "404";
		restMessage.message = "User deletion failed.";
		return ResponseEntity.status(404).body(restMessage);
	}

	/*
	 * get user by mobile
	 */
//	@RequestMapping(path = "/mobile", method = RequestMethod.GET)
//	public ResponseEntity<User> getUserByMobile(@RequestParam long mobile) {
//		User user = userServiceImpl.getUserByMobile(mobile);
//		System.out.println(user);
//		if (user == null) {
//			return ResponseEntity.status(200).body(user);
//		}
//		return ResponseEntity.status(200).body(user);
//	}

	/*
	 * get user by mobile and email
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<User> getUserByMobileAndEmail(@RequestParam long mobileNumber, @RequestParam String email) {
		User user = userServiceImpl.getUserByMobileAndEmail(mobileNumber, email);
		System.out.println(user);
		if (user == null) {
			return ResponseEntity.status(204).body(user);
			
		}
		return ResponseEntity.status(200).body(user);
	}

}
